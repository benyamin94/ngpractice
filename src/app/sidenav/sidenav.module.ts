import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatSidenavModule,
  MatToolbarModule,
  MatButtonModule,
  MatIconModule,
  MatListModule,
  MatExpansionModule
} from '@angular/material';

import { SidenavComponent } from './sidenav.component';
import {RouterModule} from "@angular/router";


@NgModule({
  declarations: [
    SidenavComponent,
  ],
  imports: [
    CommonModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatIconModule,
    MatExpansionModule,
    MatButtonModule,
    RouterModule
  ],
  exports: [SidenavComponent]
})
export class SidenavModule { }

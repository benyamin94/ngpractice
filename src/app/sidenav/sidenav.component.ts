import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css'],
})
export class SidenavComponent {
  buttons = [
    { name: 'Open', link: '#' },
    { name: 'Open1', link: '#', sub: [
      { name: 'Sub2', link: '#' },
      { name: 'sub3', link: '#' },
      { name: 'sub6', link: '#' }
    ]},
    { name: 'Open2', link: '#' },
    { name: 'Open3', link: '#' },
    { name: 'Open4', link: '#' },
    { name: 'Open5', link: '#', sub: [
      { name: '123qwe4r5t6y', link: '#' }
    ]}
  ];

  constructor() {}

}

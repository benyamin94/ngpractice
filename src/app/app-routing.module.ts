import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {Routes, RouterModule} from '@angular/router';


const routes: Routes = [
  {
    path: 'campaing',
    children: [
      {
        path: 'segments',
        loadChildren: './segments/segments.module#SegmentsModule'
      },
      {
        path: 'devices',
        loadChildren: './devices/devices.module#DevicesModule'
      }
    ]
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

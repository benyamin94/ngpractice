import { Component, OnInit } from '@angular/core';
import {DeviceModel} from "../device.model";
import {Subscription} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import {DeviceService} from "../device.service";
import {SnackbarService} from "../../snackbar.service";
import {MatDialog} from "@angular/material";
import {DeviceEditComponent} from "../device-edit/device-edit.component";

@Component({
  selector: 'app-device-detail',
  templateUrl: './device-detail.component.html',
  styleUrls: ['./device-detail.component.css']
})
export class DeviceDetailComponent implements OnInit {
  device: DeviceModel;
  deviceSubscr: Subscription;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private deviceService: DeviceService,
    private snackBarService: SnackbarService,
    private dialog: MatDialog
  ) { }

  loadSegment() {
    this.deviceSubscr = this.deviceService.getById(
      +this.route.snapshot.params.id
    ).subscribe( (device: DeviceModel) => this.device = device );
  }

  ngOnInit() {
    this.loadSegment();
  }

  openEditDialog() {
    this.dialog.open(
      DeviceEditComponent,
      { data: {device: this.device}, width: '700px', height: '400px' }
    ).afterClosed().subscribe( (msg: string) => {
      if (msg) { this.snackBarService.alertMsg(msg); }
    });
  }

}

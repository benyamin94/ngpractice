import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatTableDataSource} from '@angular/material';
import {Subscription} from 'rxjs';

import {DeviceModel} from '../device.model';
import {DeviceService} from '../device.service';
import {SnackbarService} from '../../snackbar.service';
import {DeviceEditComponent} from '../device-edit/device-edit.component';
import {DeviceRemoveComponent} from '../device-remove/device-remove.component';


@Component({
  selector: 'app-device-list',
  templateUrl: './device-list.component.html',
  styleUrls: ['./device-list.component.css']
})
export class DeviceListComponent implements OnInit, OnDestroy {

  displayedColumns = ['type', 'description', 'segment', 'actions'];
  dataSource: MatTableDataSource<DeviceModel>;
  dataSubscr: Subscription;

  @ViewChild(MatPaginator)
  paginator: MatPaginator;

  constructor(
    private deviceService: DeviceService,
    private snackBarService: SnackbarService,
    private dialog: MatDialog
  ) {
  }

  loadData(silent: boolean = false) {
    this.dataSubscr = this.deviceService.getAll().subscribe(
      (devices: DeviceModel[]) => {
        console.log(devices);
        this.dataSource = new MatTableDataSource<DeviceModel>(devices);
        if (!silent) {
          this.snackBarService.alertMsg(`Succesfully loaded ${this.dataSource.data.length} devices.`);
        }
        this.dataSource.paginator = this.paginator;
      }
    );
  }

  ngOnInit() {
    this.loadData();
  }

  openCreateEditDialog(device?: DeviceModel) {
    this.dialog.open(
      DeviceEditComponent,
      {data: device ? {device} : {}, width: '700px', height: '400px'}
    ).afterClosed().subscribe(
      (msg: string) => {
        if (msg) {
          this.snackBarService.alertMsg(msg);
        }
        this.loadData(true);
      }
    );
  }

  openRemoveDialog(device: DeviceModel) {
    this.dialog.open(
      DeviceRemoveComponent,
      {data: device, width: '400ps', height: '200px'}
    ).afterClosed().subscribe(
      (res: { deleted: boolean, msg: string }) => {
        if (res.deleted) {
          this.snackBarService.alertMsg(res.msg);
        }
        this.loadData(true);
      }
    );
  }

  ngOnDestroy(): void {
    this.dataSubscr.unsubscribe();
  }

}

export interface DeviceModel {
  id?: number;
  description?: string;
  deviceType?: 'SMARTPHONE' | 'FEATUREPHONE';
  segment?: number;
}

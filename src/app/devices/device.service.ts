import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';

import {DeviceModel} from './device.model';
import * as faker from 'faker';


@Injectable({
  providedIn: 'root'
})
export class DeviceService {
  devices: DeviceModel[] = [];

  constructor() {
    for (let i = 0; i < 20; i++) {
      this.devices.push({
        id: i,
        deviceType: faker.random.arrayElement(['SMARTPHONE', 'FEATUREPHONE']) as any,
        description: faker.random.words(5),
        segment: faker.random.number(20)
      });
    }
  }

  getAll(): Observable<DeviceModel[]> {
    return of(this.devices);
  }

  getById(id: number): Observable<DeviceModel> {
    return of(this.devices.find( (device: DeviceModel) => device.id === id ));
  }

  getIndexById(id: number): number {
    return this.devices.findIndex((result: DeviceModel) => result.id === id);
  }

  update(device: DeviceModel) {
    const index = this.getIndexById(device.id);
    this.devices[index] = device;
  }

  delete(device: DeviceModel) {
    const index = this.getIndexById(device.id);
    if (index !== -1) { this.devices.splice(index, 1); }
  }

  create(device: DeviceModel) {
    device.id = this.devices.length + 1;
    this.devices.unshift(device);
  }
}


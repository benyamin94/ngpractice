import {Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from '@angular/material';
import {FormControl} from '@angular/forms';

import {SegmentModel} from '../segment.model';
import {SegmentService} from '../segment.service';


@Component({
  selector: 'app-segment-remove',
  templateUrl: './segment-remove.component.html',
  styleUrls: ['./segment-remove.component.css']
})
export class SegmentRemoveComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: SegmentModel,
    public dialogRef: MatDialogRef<SegmentRemoveComponent>,
    private segmentService: SegmentService,
    private snackBar: MatSnackBar
  ) {
  }

  ngOnInit() {
  }

  remove() {
    this.segmentService.delete(this.data);
    this.dialogRef.close(`Succesfully deleted segment ${this.data.name}.`);
  }
}

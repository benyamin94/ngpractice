import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';

import {SegmentModel} from './segment.model';
import * as faker from 'faker';


@Injectable({
  providedIn: 'root'
})
export class SegmentService {
  segments: SegmentModel[] = [];
  constructor() {
    for (let i = 0; i < 20; i++) {
      this.segments.push(this.genRandomSegment(i));
    }
  }

  genRandomSegment(i: number): SegmentModel {
    return {
      description: faker.company.companyName(),
      id: i,
      name: faker.random.word(),
      status: faker.random.word()
    };
  }

  getAll(): Observable<SegmentModel[]> {
    return of(this.segments);
  }

  getById(id: number): Observable<SegmentModel> {
    const segment = this.segments.find( (result: SegmentModel) => result.id === id );
    return of(segment);
  }

  getIndexById(id: number): number {
    return this.segments.findIndex((result: SegmentModel) => result.id === id);
  }

  update(segment: SegmentModel) {
    const index = this.getIndexById(segment.id);
    this.segments[index] = segment;
  }

  delete(segment: SegmentModel) {
    const index = this.getIndexById(segment.id);
    if (index !== -1) {
      this.segments.splice(index, 1);
    }
  }

  create(segment: SegmentModel) {
    segment.id = this.segments.length + 1;
    this.segments.unshift(segment);
  }
}

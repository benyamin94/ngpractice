import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatPaginatorModule,
  MatTableModule,
  MatDialogModule,
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatCardModule,
  MatSnackBarModule
} from '@angular/material';
import { LayoutModule } from '@angular/cdk/layout';
import { ReactiveFormsModule } from '@angular/forms';

import { SegmentRoutingModule } from './segment-routing.module';

import { SegmentListComponent } from './segment-list/segment-list.component';
import { SegmentDetailComponent } from './segment-detail/segment-detail.component';
import { SegmentEditComponent } from './segment-edit/segment-edit.component';
import { SegmentRemoveComponent } from './segment-remove/segment-remove.component';


@NgModule({
  declarations: [
    SegmentListComponent,
    SegmentDetailComponent,
    SegmentEditComponent,
    SegmentRemoveComponent,
  ],
  imports: [
    CommonModule,
    LayoutModule,
    MatTableModule,
    MatPaginatorModule,
    MatDialogModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatSnackBarModule,
    SegmentRoutingModule
  ],
  providers: [],
  entryComponents: [
    SegmentEditComponent,
    SegmentRemoveComponent,
  ]
})
export class SegmentsModule { }

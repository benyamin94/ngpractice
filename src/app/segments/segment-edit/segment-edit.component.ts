import { Component, OnInit, Inject } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import { FormControl, FormGroup } from '@angular/forms';

import {SegmentModel} from '../segment.model';
import {SegmentService} from '../segment.service';
import {SnackbarService} from '../../snackbar.service';

@Component({
  selector: 'app-segment-edit',
  templateUrl: './segment-edit.component.html',
  styleUrls: ['./segment-edit.component.css']
})
export class SegmentEditComponent implements OnInit {
  segmentForm: FormGroup;
  idValue: number;
  nameValue = '';
  statusValue = '';
  descriptionValue = '';

  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: {segment?: SegmentModel},
    public dialogRef: MatDialogRef<SegmentEditComponent>,
    private segmentService: SegmentService
  ) { }

  ngOnInit() {
    if (this.data.segment) {
      this.idValue = this.data.segment.id;
      this.nameValue = this.data.segment.name;
      this.statusValue = this.data.segment.status;
      this.descriptionValue = this.data.segment.description;
    }

    this.segmentForm = new FormGroup({
      id: new  FormControl(this.idValue),
      name: new FormControl(this.nameValue),
      status: new FormControl(this.statusValue),
      description: new FormControl(this.descriptionValue)
    });
  }

  save() {
    if (this.data.segment) {
      this.segmentService.update(this.segmentForm.value);
    } else {

      this.segmentService.create(this.segmentForm.value);
    }
    const msg = `Segment ${this.segmentForm.controls.name.value} ${this.data.segment ? 'updated' : 'created'}!`;
    this.dialogRef.close(msg);
  }

}

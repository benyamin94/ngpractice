import { NgModule } from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {SegmentListComponent} from './segment-list/segment-list.component';
import {SegmentDetailComponent} from './segment-detail/segment-detail.component';


const routes: Routes = [
  {path: '', component: SegmentListComponent},
  {path: ':id', component: SegmentDetailComponent}
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SegmentRoutingModule { }

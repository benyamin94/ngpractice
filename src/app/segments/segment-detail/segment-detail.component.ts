import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';

import { SegmentModel } from '../segment.model';
import { SegmentService } from '../segment.service';
import {Observable, Subscription} from 'rxjs';
import {MatDialog} from '@angular/material';
import {SegmentEditComponent} from '../segment-edit/segment-edit.component';
import {SegmentRemoveComponent} from '../segment-remove/segment-remove.component';
import {SnackbarService} from '../../snackbar.service';

@Component({
  selector: 'app-segment-detail',
  templateUrl: './segment-detail.component.html',
  styleUrls: ['./segment-detail.component.css']
})
export class SegmentDetailComponent implements OnInit, OnDestroy {
  segment: SegmentModel;
  id: number = +this.route.snapshot.params.id;

  segmentSubscr: Subscription;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private segmentService: SegmentService,
    private snackBarService: SnackbarService,
    public dialog: MatDialog
  ) { }

  loadSegment() {
    this.segmentSubscr = this.segmentService.getById(this.id).subscribe(
      (segment: SegmentModel) => this.segment = segment );
  }

  ngOnInit() {
    this.loadSegment();
  }

  ngOnDestroy(): void {
    this.segmentSubscr.unsubscribe();
  }

  openEditDialog() {
    this.dialog.open(
      SegmentEditComponent,
      { data: {segment: this.segment}, width: '700px', height: '400px' })
      .afterClosed().subscribe((msg: string) => {
        if (msg) { this.snackBarService.alertMsg(msg); }
        this.loadSegment();
    });
  }

  openRemoveDialog() {
    this.dialog.open(
      SegmentRemoveComponent,
      { data: this.segment, width: '400px', height: '200px' }
    ).afterClosed().subscribe(
      (msg: string) => {
        if (msg) {
          this.snackBarService.alertMsg(msg);
          this.router.navigate(['/campaing/segments']);
        } else { this.loadSegment(); }
        }
    );
  }


}

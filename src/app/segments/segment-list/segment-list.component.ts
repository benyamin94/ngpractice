import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {
  MatDialog,
  MatPaginator,
  MatTableDataSource
} from '@angular/material';

import {SegmentModel} from '../segment.model';
import { SnackbarService } from '../../snackbar.service';
import {SegmentService} from '../segment.service';
import {SegmentEditComponent} from '../segment-edit/segment-edit.component';
import {SegmentRemoveComponent} from '../segment-remove/segment-remove.component';
import {Subscription} from "rxjs";


@Component({
  selector: 'app-main-content',
  templateUrl: './segment-list.component.html',
  styleUrls: ['./segment-list.component.css'],
})
export class SegmentListComponent implements OnInit , OnDestroy {

  displayedColumns: string[] = ['name', 'status', 'description', 'actions'];
  dataSource: MatTableDataSource<SegmentModel>;
  dataSubscr: Subscription;

  constructor(
    private segmentService: SegmentService,
    private snackBarService: SnackbarService,
    public dialog: MatDialog
  ) {
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;

  loadData(silent: boolean = false) {
    this.dataSubscr = this.segmentService.getAll().subscribe(
      (segments: SegmentModel[]) => {
        this.dataSource = new MatTableDataSource<SegmentModel>(segments);
        if (!silent) { this.snackBarService.alertMsg(`Succesfully loaded ${this.dataSource.data.length} segments.`); }
      });
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit() {
    this.loadData();
  }

  openCreateEditDialog(segment?: SegmentModel) {
    this.dialog.open(
      SegmentEditComponent,
      { data: segment ? {segment} : {}, width: '700px', height: '400px' }
      ).afterClosed().subscribe(
      (msg: string) => {
        if (msg) { this.snackBarService.alertMsg(msg); }
        this.loadData(true);
      }
    );
  }

  openRemoveDialog(segment: SegmentModel) {
    this.dialog.open(
      SegmentRemoveComponent,
      {data: segment, width: '400px', height: '200px'}
    ).afterClosed().subscribe(
      (res: {deleted: boolean, msg: string}) => {
        if (res.deleted) { this.snackBarService.alertMsg(res.msg); }
        this.loadData(true);
      }
    );
  }

  ngOnDestroy(): void {
    this.dataSubscr.unsubscribe();
  }

}

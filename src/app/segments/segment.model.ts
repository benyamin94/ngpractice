export interface SegmentModel {
  id?: number;
  name?: string;
  description?: string;
  status?: string;
}

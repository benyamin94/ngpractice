import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {MatSnackBarContainer, MatSnackBarModule} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { SidenavModule } from './sidenav/sidenav.module';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';

import { SnackbarService } from './snackbar.service';
import { SegmentService } from './segments/segment.service';


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    SidenavModule,
    AppRoutingModule,
    MatSnackBarModule
  ],
  providers: [SnackbarService, SegmentService],
  bootstrap: [AppComponent],
  entryComponents: [
    MatSnackBarContainer
  ]
})
export class AppModule { }
